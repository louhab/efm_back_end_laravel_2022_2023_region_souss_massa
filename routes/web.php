<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LivreController ;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


    Route::get('/livres', [LivreController::class, 'index']);
    Route::get('/livres/create', [LivreController::class, 'create']);
    Route::post('/livres', [LivreController::class, 'store']);
    Route::get('/livres/{id}/edit', [LivreController::class, 'edit']);
    Route::put('/livres/{id}', [LivreController::class, 'update']);
    Route::delete('/livres/{id}', [LivreController::class, 'destroy']);





