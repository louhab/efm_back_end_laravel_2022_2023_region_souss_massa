# Laravel Application README

## Exercise 1: Travail à faire :

### Questions
1. **1. Écrire la commande artisan pour créer les modèles "Livre" et "Catégorie".?**
   - php artisan make:model Livre -m
   - php artisan make:model Categorie -m


2. **Donner les lignes de code à ajouter au modèle "Livre" afin que tous les attributs puissent être affectés?**
  
   - protected $fillable = ['titre', 'pages', 'description', 'image', 'categorie_id'];

3. **Code de la méthode "livres" à ajouter au modèle "Catégorie"?**
   
   -  
   public function livres()
    {
        return $this->hasMany(Livre::class, 'categorie_id');
    }

4. **Code de la méthode "up" du fichier de migration correspondant au modèle "Livre"?**

    public function up()
    {
        Schema::create('livres', function (Blueprint $table) {
            $table->id();
            $table->string('titre');
            $table->decimal('pages');
            $table->text('description');
            $table->string('image')->nullable();
            $table->unsignedBigInteger('categorie_id');
            $table->timestamps();
        });
    }
4. **Commande artisan pour créer le contrôleur "LivreController"?**

  - php artisan make:controller LivreController

5. **Méthodes du contrôleur "LivreController":?**:

    public function index()
    {
        // Code pour récupérer la liste des livres et renvoyer la vue "index"
    }

    public function create()
    {
        // Code pour afficher le formulaire de création de livre (vue "create")
    }

    public function store(Request $request)
    {
        // Code pour valider et sauvegarder les données du formulaire
    }

    public function edit($id)
    {
        // Code pour récupérer les informations d'un livre et renvoyer la vue "edit"
    }

    public function update(Request $request, $id)
    {
        // Code pour valider et mettre à jour les données d'un livre
    }

    public function destroy($id)
    {
        // Code pour supprimer un livre
    }

6. **Routes pour le contrôleur "LivreController"::?**:

    Route::get('/livres', 'LivreController@index');
    Route::get('/livres/create', 'LivreController@create');
    Route::post('/livres', 'LivreController@store');
    Route::get('/livres/{id}/edit', 'LivreController@edit');
    Route::put('/livres/{id}', 'LivreController@update');
    Route::delete('/livres/{id}', 'LivreController@destroy');

6. **Code de la vue "index"?**:

    @extends('layouts.app')
    @section('content')
        <table>
            <!-- Code pour afficher tous les livres -->
        </table>
    @endsection
6. **Code de la colonne "Actions" avec les boutons "ajouter", "éditer" et "supprimer"**:
7. **Règles de validation pour le formulaire de création de livre dans la méthode "store":**:

    $request->validate([
        'titre' => 'required|max:255',
        'pages' => 'required|integer|min:1',
        'description' => 'required',
        'categorie_id' => 'required|exists:categories,id',
    ]);

7. **Commande artisan pour créer le middleware "AuthMiddleware":**:

    php artisan make:middleware AuthMiddleware.

8. **Appliquer le middleware "AuthMiddleware" sur les routes du contrôleur "LivreController" dans le fichier des routes**:

9. **Fonctionnalité supplémentaire pour télécharger une image:**

    -- **a. Commande artisan pour ajouter une migration:**:

    -- php artisan make:migration add_image_to_livres_table --table=livres

    -- **b. Code des fonctions "up" et "down" de la migration:**:

    public function up()
        {
            Schema::table('livres', function (Blueprint $table) {
                $table->string('image')->nullable();
            });
        }
    public function down()
        {
            Schema::table('livres', function (Blueprint $table) {
                $table->dropColumn('image');
            });
        }
