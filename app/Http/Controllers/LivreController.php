<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class LivreController extends Controller
{
    
    public function index()
    {
        // Code pour récupérer la liste des livres et renvoyer la vue "index"
    }

    public function create()
    {
        // Code pour afficher le formulaire de création de livre (vue "create")
    }

    public function store(Request $request)
    {
        // Code pour valider et sauvegarder les données du formulaire

        $request->validate([
            'titre' => 'required|max:255',
            'pages' => 'required|integer|min:1',
            'description' => 'required',
            'categorie_id' => 'required|exists:categories,id',
        ]);
    }

    public function edit($id)
    {
        // Code pour récupérer les informations d'un livre et renvoyer la vue "edit"
    }

    public function update(Request $request, $id)
    {
        // Code pour valider et mettre à jour les données d'un livre
    }

    public function destroy($id)
    {
        // Code pour supprimer un livre
    }
}
